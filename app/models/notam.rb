class Notam
  attr_accessor :content
  attr_reader :icao_code, :service_hours

  DAY_MAPPING = {"MON" => 0, "TUE" => 1, "WED" => 2,
                 "THU" => 3, "FRI" => 4, "SAT" => 5, "SUN" => 6}

  def initialize(text)
    @content = text.freeze

    @aerodrome_hours_of_service = "AERODROME HOURS OF OPS/SERVICE".in?(@content)

    a_section = @content.lines.map(&:chomp)[2]
    @icao_code = a_section[/A\) *([A-Z]+).*/, 1]
  end

  def hours_of_service_notice?
    @aerodrome_hours_of_service
  end

  def service_hours
    @service_hours ||= parse_service_hours
  end

  def parse_service_hours
    unless hours_of_service_notice?
      return
    end

    cleaned = @content
              .gsub(/[ \.,\n]+/, " ")
    times = cleaned[/E\).?AERODROME HOURS OF OPS\/SERVICE.?(.+) *CREATED.*$/, 1]
            .strip
            .split

    # This is slightly unfortunate -- the sample data contains one instance of
    # an opening time, where the time is SAT0630-0730, i.e no space between the
    # day and the time.
    times = times.map do |t|
      # If one of the times parsed contains *both* numbers and characters, break
      # it up into two parts -- day and opening time
      if !!(t[/[A-Z]+/] && t[/[\d]+/])
        [t[/[A-Z\-]+/], t[/\d{4}\-\d{4}/]]
      else
        t
      end
    end.flatten

    hours_of_service = {}
    while !times.empty?
      days = times.first
      hours = times.drop(1).take_while { |x| x =~ /\d{4}\-\d{4}/ }
      if hours.empty?
        hours = [times.drop(1).first]
      end
      times = times.drop(1 + hours.size)
      hours_of_service[days] = hours
    end

    expanded = {}
    hours_of_service.each_pair do |key, value|
      expanded.merge!(expand_keys_with_value(key, value))
    end
    expanded
  end

  def expand_keys_with_value(day_or_range, open_time)
    if "-".in?(day_or_range)
      days = day_or_range.split("-")
      range_start, range_end = DAY_MAPPING[days.first], DAY_MAPPING[days.last]
      Hash[(range_start..range_end).map { |i| [i, open_time] }]
    else
      { DAY_MAPPING[day_or_range] => open_time }
    end
  end
end
