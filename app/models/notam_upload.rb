require 'active_model'

class NotamUpload
  include ::ActiveModel::Validations

  attr_accessor :notams, :file_content

  validates_format_of :file_content, with: /\n\n+/

  def initialize(ns)
    @file_content = ns.read
  end

  def notams
    @notams ||= self.file_content.split("\n\n").map { |n| Notam.new(n) }
  end

  def notams_about_aerodrome_hours_of_service
    self.notams.select(&:hours_of_service_notice?)
  end
end
