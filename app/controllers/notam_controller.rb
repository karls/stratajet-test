class NotamController < ApplicationController
  before_action :check_file_upload, only: [:notams]

  def index
  end

  def upload
    uploaded_io = params[:notams_file]
    filename = session.id + "_" + uploaded_io.original_filename
    filepath = Rails.root.join('public', 'uploads', filename).to_s
    session[:uploaded_file_name] = filepath

    File.open(filepath, 'wb') do |file|
      file.write(uploaded_io.read)
    end

    redirect_to notams_path
  end

  def notams
    f = File.open(session[:uploaded_file_name])
    upload = NotamUpload.new(f)
    session[:uploaded_file_name] = nil

    unless upload.valid?
      flash[:error] = upload.errors.full_messages.join(". ")
      render file: 'public/422.html', status: 422, layout: false
      return
    end

    @notams = upload.notams_about_aerodrome_hours_of_service
  end

  private

  def error422
    render :status => 422, :formats => [:html]
  end

  def check_file_upload
    unless session[:uploaded_file_name]
      flash[:info] = "Please upload a file first"
      redirect_to root_path
      return
    end
  end
end
