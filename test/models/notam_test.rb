require 'test_helper'

class NotamTest < ActiveSupport::TestCase
  notice = <<-NOTAM
B0519/15 NOTAMN
Q) ESAA/QFAAH/IV/NBO/A /000/999/5746N01404E005
A) ESGT B) 1502271138 C) 1503012359
E) AERODROME HOURS OF OPS/SERVICE MON-WED 0500-1830 THU 0500-2130
FRI
0730-2100 SAT 0630-0730, 1900-2100 SUN CLOSED
CREATED: 27 Feb 2015 11:40:00
SOURCE: EUECYIYN
NOTAM

  other_notice = <<-NOTAM
B0517/15 NOTAMN
Q) ESAA/QSTAH/IV/BO /A /000/999/5746N01404E005
A) ESGJ B) 1502271133 C) 1503012359
E) AERODROME CONTROL TOWER (TWR) HOURS OF OPS/SERVICE MON-THU
0000-0100, 0500-2359 FRI 0000-0100, 0730-2100 SAT 0630-0730,
1900-2100 SUN 2200-2359
CREATED: 27 Feb 2015 11:35:00
SOURCE: EUECYIYN
NOTAM

  weird_notice = <<-NOTAM
SWES0196 ESSV 02271450
(SNOWTAM 0196
A)ESSV
B)02271450
C)03 F)NIL/NIL/NIL)
CREATED: 27 Feb 2015 15:05:00
SOURCE: EUECYIYN
NOTAM

  test "initialising" do
    notam = Notam.new(notice)
    assert_equal notam.content, notice
    assert_raises RuntimeError do
      notam.content[0] = "X"
    end
  end

  test "aerodrome_hours_of_service?" do
    notam = Notam.new(notice)
    assert_equal(true, notam.hours_of_service_notice?)
  end

  test "aerodrome_hours_of_service? for other notice" do
    notam = Notam.new(other_notice)
    assert_equal(false, notam.hours_of_service_notice?)
  end

  test "aerodrome_hours_of_service? for weird notice" do
    notam = Notam.new(weird_notice)
    assert_equal(false, notam.hours_of_service_notice?)
  end

  test "icao_code for a notice" do
    notam = Notam.new(notice)
    assert("ESGT", notam.icao_code)
  end

  test "icao_code for other notice" do
    notam = Notam.new(other_notice)
    assert("ESGJ", notam.icao_code)
  end

  test "icao_code for werid notice" do
    notam = Notam.new(weird_notice)
    assert_equal("ESSV", notam.icao_code)
  end

  test "expand_keys_with_value simple case" do
    notam = Notam.new(notice)
    open_times = notam.expand_keys_with_value("MON", "0600-2100")
    assert_equal({0 => "0600-2100"},
                 open_times)
  end

  test "expand_keys_with_value complex case" do
    notam = Notam.new(notice)
    open_times = notam.expand_keys_with_value("TUE-THU", "0600-2100")
    assert_equal({1 => "0600-2100", 2 => "0600-2100", 3 => "0600-2100"},
                 open_times)
  end

  test "hours_of_service with notice" do
    notam = Notam.new(notice)
    assert_equal({ 0 => ["0500-1830"],
                   1 => ["0500-1830"],
                   2 => ["0500-1830"],
                   3 => ["0500-2130"],
                   4 => ["0730-2100"],
                   5 => ["0630-0730", "1900-2100"],
                   6 => ["CLOSED"]
                 }, notam.service_hours)
  end

  test "hours_of_service with notice1" do
    notice1 = <<-NOTAM
B0508/15 NOTAMN
Q) ESAA/QFAAH/IV/NBO/A /000/999/5746N01404E005
A) ESGJ B) 1503020000 C) 1503222359
E) AERODROME HOURS OF OPS/SERVICE MON 0500-2000 TUE-THU 0500-2100
FRI
0545-2100 SAT0630-0730 1900-2100 SUN 1215-2000
CREATED: 26 Feb 2015 10:54:00
SOURCE: EUECYIYN
NOTAM

    notam = Notam.new(notice1)
    assert_equal({ 0 => ["0500-2000"],
                   1 => ["0500-2100"],
                   2 => ["0500-2100"],
                   3 => ["0500-2100"],
                   4 => ["0545-2100"],
                   5 => ["0630-0730", "1900-2100"],
                   6 => ["1215-2000"]
                 }, notam.service_hours)
  end

  test "hours_of_service with notice2" do
    notice2 = <<-NOTAM
B0513/15 NOTAMN
Q) ESAA/QFAAH/IV/NBO/A /000/999/6232N01727E005
A) ESNN B) 1503090000 C) 1503292359
E) AERODROME HOURS OF OPS/SERVICE
MON 0445-2115 TUE 0500-2130 WED-THU 0500-2300 FRI 0500-2115 SAT
0715-1300 SUN 1115-2145
CREATED: 27 Feb 2015 10:48:00
SOURCE: EUECYIYN
NOTAM

    notam = Notam.new(notice2)
    assert_equal({ 0 => ["0445-2115"],
                   1 => ["0500-2130"],
                   2 => ["0500-2300"],
                   3 => ["0500-2300"],
                   4 => ["0500-2115"],
                   5 => ["0715-1300"],
                   6 => ["1115-2145"]
                 }, notam.service_hours)
  end

  test "hours_of_service with notice3" do
    notice3 = <<-NOTAM
B0511/15 NOTAMN
Q) ESAA/QFAAH/IV/NBO/A /000/999/6232N01727E005
A) ESNN B) 1503020000 C) 1503082359
E) AERODROME HOURS OF OPS/SERVICE
MON 0500-2215 TUE 0500-2130 WED-THU 0500-2300 FRI 0500-2015 SAT
1100-1300 SUN 1115-2300
CREATED: 27 Feb 2015 10:41:00
SOURCE: EUECYIYN
NOTAM

    notam = Notam.new(notice3)
    assert_equal({ 0 => ["0500-2215"],
                   1 => ["0500-2130"],
                   2 => ["0500-2300"],
                   3 => ["0500-2300"],
                   4 => ["0500-2015"],
                   5 => ["1100-1300"],
                   6 => ["1115-2300"]
                 }, notam.service_hours)
  end

  test "hours_of_service with notice4" do
    notice4 = <<-NOTAM
B0274/15 NOTAMN
Q) ESAA/QFAAH/IV/NBO/A /000/999/6232N01727E005
A) ESNN B) 1502230000 C) 1503012359
E) AERODROME HOURS OF OPS/SERVICE
MON 0500-2215 TUE 0500-2130 WED-THU 0500-2300 FRI 0500-1830 SAT
1100-1300 SUN 1115-2145
CREATED: 30 Jan 2015 14:12:00
SOURCE: EUECYIYN
NOTAM

    notam = Notam.new(notice4)
    assert_equal({ 0 => ["0500-2215"],
                   1 => ["0500-2130"],
                   2 => ["0500-2300"],
                   3 => ["0500-2300"],
                   4 => ["0500-1830"],
                   5 => ["1100-1300"],
                   6 => ["1115-2145"]
                 }, notam.service_hours)
  end

  test "hours_of_service with notice5" do
    notice5 = <<-NOTAM
B0451/15 NOTAMN
Q) ESAA/QFAAH/IV/NBO/A /000/999/6324N01900E005
A) ESNO B) 1503090000 C) 1503292359
E) AERODROME HOURS OF OPS/SERVICE:MON-WED 0430-2130 THU 0430-2215
FRI 0445-2130 SAT CLSD SUN 1030-1900
CREATED: 22 Feb 2015 13:17:00
SOURCE: EUECYIYN
NOTAM

    notam = Notam.new(notice5)
    assert_equal({ 0 => ["0430-2130"],
                   1 => ["0430-2130"],
                   2 => ["0430-2130"],
                   3 => ["0430-2215"],
                   4 => ["0445-2130"],
                   5 => ["CLSD"],
                   6 => ["1030-1900"]
                 }, notam.service_hours)
  end

  test "hours_of_service with notice6" do
    notice6 = <<-NOTAM
B0267/15 NOTAMN
Q) ESAA/QFAAH/IV/NBO/A /000/999/6324N01900E005
A) ESNO B) 1502090000 C) 1503082359
E) AERODROME HOURS OF OPS/SERVICE:MON 0530-2030 TUE-WED 0530-2230
THU
0530-2315 FRI 0545-2230 SAT CLSD SUN 1130-2000
CREATED: 30 Jan 2015 11:18:00
SOURCE: EUECYIYN
NOTAM

    notam = Notam.new(notice6)
    assert_equal({ 0 => ["0530-2030"],
                   1 => ["0530-2230"],
                   2 => ["0530-2230"],
                   3 => ["0530-2315"],
                   4 => ["0545-2230"],
                   5 => ["CLSD"],
                   6 => ["1130-2000"]
                 }, notam.service_hours)
  end

  test "hours_of_service with notice7" do
    notice7 = <<-NOTAM
B0373/15 NOTAMN
Q) ESAA/QFAAH/IV/NBO/A /000/999/6312N01430E005
A) ESNZ B) 1502230000 C) 1503222359
E) AERODROME HOURS OF OPS/SERVICE MON-TUE 0515-2145 WED-THU
0515-2255
FRI 0515-2145 SAT 0645-1845 SUN 0645-2145
CREATED: 13 Feb 2015 09:46:00
SOURCE: EUECYIYN
NOTAM

    notam = Notam.new(notice7)
    assert_equal({ 0 => ["0515-2145"],
                   1 => ["0515-2145"],
                   2 => ["0515-2255"],
                   3 => ["0515-2255"],
                   4 => ["0515-2145"],
                   5 => ["0645-1845"],
                   6 => ["0645-2145"]
                 }, notam.service_hours)
  end

  test "hours_of_service with notice8" do
    notice8 = <<-NOTAM
B0296/15 NOTAMN
Q) ESAA/QFAAH/IV/NBO/A /000/999/5740N01821E005
A) ESSV B) 1503130000 C) 1503292359
E) AERODROME HOURS OF OPS/SERVICE. MON-THU 0530-2115, FRI 0530-2000,
SAT 0730-1600, SUN 1000-2115.
CREATED: 02 Feb 2015 12:20:00
SOURCE: EUECYIYN
NOTAM

    notam = Notam.new(notice8)
    assert_equal({ 0 => ["0530-2115"],
                   1 => ["0530-2115"],
                   2 => ["0530-2115"],
                   3 => ["0530-2115"],
                   4 => ["0530-2000"],
                   5 => ["0730-1600"],
                   6 => ["1000-2115"]
                 }, notam.service_hours)
  end

  test "hours_of_service with notice9" do
    notice9 = <<-NOTAM
B0295/15 NOTAMN
Q) ESAA/QFAAH/IV/NBO/A /000/999/5740N01821E005
A) ESSV B) 1502210000 C) 1503122359
E) AERODROME HOURS OF OPS/SERVICE. MON-THU 0530-2115, FRI 0530-2000,
SAT 0730-1830, SUN 0730-2115.
CREATED: 02 Feb 2015 12:17:00
SOURCE: EUECYIYN
NOTAM

    notam = Notam.new(notice9)
    assert_equal({ 0 => ["0530-2115"],
                   1 => ["0530-2115"],
                   2 => ["0530-2115"],
                   3 => ["0530-2115"],
                   4 => ["0530-2000"],
                   5 => ["0730-1830"],
                   6 => ["0730-2115"]
                 }, notam.service_hours)
  end
end
