require 'test_helper'

class NotamUploadTest < ActiveSupport::TestCase
  setup do
    @upload = NotamUpload.new(File.new("./test/data/notam_data.txt"))
  end

  test "initialize from IO-like" do
    assert(@upload.valid?)
  end

  test "initialize from IO-like with a bad file" do
    assert(!NotamUpload.new(File.open("./test/data/bad_data.txt")).valid?)
  end

  test "select aerodrome hours of service notams" do
    assert_equal(10, @upload.notams_about_aerodrome_hours_of_service.length)
  end
end
