require 'test_helper'

class NotamControllerTest < ActionController::TestCase
  test "gets index" do
    get :index
    assert_response :success
  end

  test "upload notams file" do
    file_to_upload = "./test/data/notam_data.txt"
    file = Rack::Test::UploadedFile.new(file_to_upload, "text/plain")
    post :upload, {notams_file: file}
    assert_redirected_to notams_path
  end

  test "upload bad file" do
    file_to_upload = "./test/data/bad_data.txt"
    file = Rack::Test::UploadedFile.new(file_to_upload, "text/plain")
    post :upload, {notams_file: file}
    assert_redirected_to notams_path
  end

  test "redirects when going straight to /notams" do
    get :notams
    assert_redirected_to root_path
    assert_equal 'Please upload a file first', flash[:info]
  end

  test "view notams file" do
    get :notams, {}, {uploaded_file_name: "./test/data/notam_data.txt"}
    assert_response :success
  end

  test "view non-existent notams file" do
    get :notams, {}, {uploaded_file_name: "./does/not/exist.txt"}
    assert_response 404
  end

end
